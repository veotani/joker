const path = require('path');
const webpack = require('webpack');

module.exports = env => {
    return {
        mode: "production",
        entry: ['babel-polyfill', path.resolve(__dirname, 'frontend/src/index.js')],
        output: {
            path: path.resolve(__dirname, "frontend/static/frontend/public/"),
            publicPath: "/static/frontend/public/",
            filename: 'main.js',
        },
        plugins: [
            new webpack.DefinePlugin({"process.env": JSON.stringify(env)})
        ],
        module: {
            rules: [
                {
                    test: /\.(js|jsx)?$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                        options: {presets: ["@babel/env"]}
                    },
                }
            ],
        },
    };
}