"""
joker URL Configuration
"""

from django.contrib import admin
from django.urls import path, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from . import settings

urlpatterns = [
    path('jokes/', include('jokes.urls')),
    path('admin/', admin.site.urls),
    path('users/', include('users.urls')),
    path('', include('frontend.urls')),
]

urlpatterns += staticfiles_urlpatterns()
