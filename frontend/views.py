from django.shortcuts import render


# React entrance
def index(request):
    return render(request, 'frontend/index.html')
