export function authorized() {
    const refreshToken = localStorage.getItem('refresh_token');
    const accessToken = localStorage.getItem('access_token');
    if (refreshToken == null || accessToken == null) {
        return false;
    }
    return true;
}