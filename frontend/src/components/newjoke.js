import React, { Component } from "react";
import axiosInstance from "../axiosApi";

class NewJoke extends Component {
    constructor(props) {
        super(props);
        this.state = {joke: ""};
        this.success = false;

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    async handleSubmit(event) {
        event.preventDefault();
        try {
            const response = await axiosInstance.post('jokes/joke/', {
                text: this.state.joke
            });
            this.success = true;
            this.setState({errors: false})
            return response.data;
        } catch (error) {
            this.success = false;
            this.setState({errors: error})
        }
    }

    render() {
        return (
            <div className="container">
                <form className="centered_form" onSubmit={this.handleSubmit}>
                    <h1>Create new joke</h1>
                    <div className="form-group">
                        <label>
                            Enter your joke text:
                            <input className="form-control" name="joke" type="text" value={this.state.joke}
                                   onChange={this.handleChange}/>
                        </label>
                    </div>

                    <input type="submit" className="btn btn-primary" value="Submit"/>
                </form>

                {this.state.errors ?
                    <div className="container alert alert-danger"><strong>Error!</strong> Try again.</div> : null}
                    {this.success ? <div className="alert alert-success" role="alert">Success!</div> : null}
            </div>
        )
    }
}

export default NewJoke;