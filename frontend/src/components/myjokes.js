import React, { Component } from "react";
import axiosInstance from "../axiosApi";

class MyJokes extends Component {
    constructor(props) {
        super(props);
        this.getJokes();
        this.state = {
            jokes: [],
            editedJoke: null,
        };

        this.handleChange = this.handleChange.bind(this);
        this.updateJoke = this.updateJoke.bind(this);
        this.deleteJoke = this.deleteJoke.bind(this);
    }

    async getJokes() {
        try {
            const response = await axiosInstance.get("jokes/jokes");
            const jokes = response.data;
            this.setState({jokes: jokes, errors: false});
        } catch (error) {
            this.setState({jokes: [], errors: error})
        }
    }

    async updateJoke() {
        try {
            const response = await axiosInstance.put("jokes/joke/", {
                id: this.state.editedJoke.id,
                text: this.state.editedJoke.text,
            });
            $("#myModal").modal('hide');
            this.getJokes();
        } catch (error) {
            this.setState({updateErrors: error})
        }
    }

    async deleteJoke() {
        try {
            const response = await axiosInstance.delete("jokes/joke/", {data:{id: this.state.editedJoke.id}});
            $("#myModal").modal('hide');
            this.getJokes();
        } catch (error) {
            this.setState({deleteErrors: error});
        }
    }

    callEditModal(joke) {
        this.setState({editedJoke: joke});
        $("#myModal").modal();
    }

    handleChange(event) {
        this.setState({editedJoke: {text: event.target.value, id: this.state.editedJoke.id}});
    }

    render() {
        const EditModal =
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit joke</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                    {
                        this.state.deleteErrors != null || this.state.updateErrors != null ?
                            <div class="container alert alert-danger"><strong>Error!</strong> Try again.</div> :
                            jokesTable
                    }
                  <div class="modal-body">
                      <input class="form-control" name="editedJokeText" type="text" value={this.state.editedJoke ? this.state.editedJoke.text : ""} onChange={this.handleChange}/>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onClick={this.updateJoke}>Save changes</button>
                    <button type="button" class="btn btn-danger" onClick={this.deleteJoke}>Delete joke</button>
                  </div>
                </div>
              </div>
            </div>

        const TableRows = this.state.jokes.map(joke =>
            <tr key={joke.id.toString()} onClick={(e) => this.callEditModal(joke)}>
                <td width={"10%"}>{joke.id}</td>
                <td width={"90%"}>{joke.text}</td>
            </tr>
        );

        const jokesTable =
            <table className="table table-hover">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Joke</th>
                </tr>
                </thead>
                <tbody>
                    {TableRows}
                </tbody>
            </table>


        return (
            <div className="container">
                {
                    this.state.errors ?
                    <div class="container alert alert-danger"><strong>Error!</strong> Try again.</div> :
                    jokesTable
                }
                { EditModal }
            </div>
        )
    }
}

export default MyJokes;