import React, { Component } from "react";
import axiosInstance from "../axiosApi";

class Signup extends Component{
    constructor(props){
        super(props);
        this.state = {
            username: "",
            password: ""
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    async handleSubmit(event) {
        event.preventDefault();
        try {
            const response = await axiosInstance.post('users/user/create/', {
                username: this.state.username,
                password: this.state.password
            });
            window.location.href = '/login/';
            return response;
        } catch (error) {
            this.setState({
                errors: error,
            });
        }
    }

    render() {
        return (
            <div className="container">
                <form className="centered_form" onSubmit={this.handleSubmit}>
                    <h1>Signup</h1>
                    <div className="form-group">
                        <label>
                            Username:
                            <input className="form-control" name="username" type="text" value={this.state.username} onChange={this.handleChange}/>
                        </label>
                    </div>

                    <div className="form-group">
                        <label>
                            Password:
                            <input className="form-control" name="password" type="password" value={this.state.password}
                                   onChange={this.handleChange}/>
                        </label>
                    </div>

                    <input type="submit" className="btn btn-primary" value="Submit"/>
                </form>
                { this.state.errors ? <div className="container alert alert-danger"><strong>Error!</strong> Try again.</div> : null}
            </div>
        )
    }
}

export default Signup;