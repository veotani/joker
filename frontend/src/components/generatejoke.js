import React, { Component } from "react";
import axiosInstance from "../axiosApi";
import axios from 'axios';

class GenerateJoke extends Component {
    constructor(props) {
        super(props);
        this.state = {joke: ""};
        this.handleSubmit = this.handleSubmit.bind(this);
        this.getJoke = this.getJoke.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        try {
            const response = await axiosInstance.post('jokes/joke/', {
                text: this.state.joke
            });
            this.success = true;
            this.setState({errors: false})
            return response.data;
        } catch (error) {
            this.success = false;
            this.setState({errors: error})
        }
    }

    async getJoke() {
        try {
            const response = await axios.get("https://geek-jokes.sameerkumar.website/api");
            this.setState({joke: response.data})
        } catch (error) {
            this.setState({errors: error})
        }
    }

    render() {
        return (
            <div className="container centered_div">
                <button onClick={this.getJoke}>Generate</button>
                {
                    this.state.joke != "" ?
                        <div className="alert alert-primary" role="alert"><div>{this.state.joke}</div><button onClick={this.handleSubmit}>Save!</button></div> :
                        null
                }
                {this.state.errors ?
                    <div className="container alert alert-danger"><strong>Error!</strong> Try again.</div> : null}
                    {this.success ? <div className="alert alert-success" role="alert">Success!</div> : null}
            </div>
        )
    }
}

export default GenerateJoke;