import React, { Component } from "react";
import axiosInstance from "../axiosApi";

class GetJoke extends Component {
    constructor(props) {
        super(props);
        this.state = {joke_id: null};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    async handleSubmit(event) {
        event.preventDefault();
        try {
            if (this.state.joke_id == null) { return; }
            const response = await axiosInstance.get('jokes/joke/?joke_id='+this.state.joke_id);
            axiosInstance.defaults.headers['Authorization'] = "JWT " + response.data.access;
            this.setState({joke: response.data.text,  errors: false});
            return response.data;
        } catch (error) {
            this.setState({joke: false, errors: error})
        }
    }

    render() {
        return (
            <div className="container">
                {this.state.joke ? <div className="alert alert-success" role="alert">{this.state.joke}</div> : null}
                <form className="centered_form" onSubmit={this.handleSubmit}>
                    <h1>Get joke by id</h1>
                    <div className="form-group">
                        <label>
                            Enter your joke id:
                            <input className="form-control" name="joke_id" type="number" value={this.state.joke_id}
                                   onChange={this.handleChange}/>
                        </label>
                    </div>

                    <input type="submit" className="btn btn-primary" value="Submit"/>
                </form>

                {this.state.errors ?
                    <div className="container alert alert-danger"><strong>Error!</strong> Try again.</div> : null}
            </div>
        )
    }
}

export default GetJoke;