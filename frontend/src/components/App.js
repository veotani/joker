import React, { Component} from "react";
import { Switch, Route, Redirect, Link } from "react-router-dom";

import Login from "./login";
import Signup from "./signup";
import Navigator from "./navigator";
import NewJoke from "./newjoke";
import GetJoke from "./getjoke"
import GenerateJoke from "./generatejoke"

import {authorized} from "../authorized";
import MyJokes from "./myjokes";

class App extends Component {
    constructor(props) {
        super(props);

        this.checkAuthorization = this.checkAuthorization.bind(this);
        this.state = {isAuthorized: authorized()};
    }

    checkAuthorization() {
        this.setState({isAuthorized: authorized()})
    }

    render() {
        return (
            <div className="site">
                <Navigator isAuthorized={this.state.isAuthorized}/>
                <main>
                    <Switch>
                        <Route
                            exact path={"/login/"}
                            render={() => authorized() ? <h2>You are logged in!</h2> : <Login checkAuthorization={this.checkAuthorization}/>}
                        />
                        <Route
                            exact path={"/signup/"}
                            render={() => authorized() ? <h2>Already logged in!</h2> : <Signup/>}
                        />
                        <Route
                            exact path={"/newjoke/"}
                            render={() => authorized() ? <NewJoke/> : <Redirect to={"/login/"} />}
                        />
                        <Route
                            exact path={"/getjoke/"}
                            render={() => authorized() ? <GetJoke/> : <Redirect to={"/login/"} />}
                        />
                        <Route
                            exact path={"/myjokes/"}
                            render={() => authorized() ? <MyJokes/> : <Redirect to={"/login/"} />}
                        />
                        <Route
                            exact path={"/generatejoke/"}
                            render={() => authorized() ? <GenerateJoke /> : <Redirect to={"/login/"} />}
                        />
                        <Route path={"/"} render={() => <h2>Home</h2>}/>
                    </Switch>
                </main>
            </div>
        );
    }
}

export default App;