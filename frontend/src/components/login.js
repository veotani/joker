import React, { Component, ReactPropTypes } from "react";
import axiosInstance from "../axiosApi";
import { authorized } from "../authorized";


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {username: "", password: ""};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    async handleSubmit(event) {
        event.preventDefault();
        try {
            const response = await axiosInstance.post('users/token/obtain/', {
                username: this.state.username,
                password: this.state.password
            });
            axiosInstance.defaults.headers['Authorization'] = "JWT " + response.data.access;
            localStorage.setItem('access_token', response.data.access);
            localStorage.setItem('refresh_token', response.data.refresh);
            this.props.checkAuthorization()
            return response.data;
        } catch (error) {
            this.setState({errors: error})
        }
    }

    render() {
        return (
            <div class="container">
                <form class="centered_form" onSubmit={this.handleSubmit}>
                    <h1>Login</h1>
                    <div class="form-group">
                    <label>
                        Username:
                        <input class="form-control" name="username" type="text" value={this.state.username} onChange={this.handleChange}/>
                    </label>
                    </div>

                    <div className="form-group">
                    <label>
                        Password:
                        <input class="form-control" name="password" type="password" value={this.state.password} onChange={this.handleChange}/>
                    </label>
                    </div>

                    <input type="submit" class="btn btn-primary" value="Submit"/>
                </form>

                { this.state.errors ? <div class="container alert alert-danger"><strong>Error!</strong> Try again.</div> : null}
            </div>

        )
    }
}

export default Login;