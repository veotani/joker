import React, { Component } from "react";
import { authorized } from "../authorized";
import {Link} from "react-router-dom";
import axiosInstance from "../axiosApi";

class Navigator extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isAuthorized: this.props.isAuthorized
        }
        this.handleLogout = this.handleLogout.bind(this);
    }

    componentWillReceiveProps(someProp) {
        this.setState({isAuthorized: someProp.isAuthorized});
    }

    async handleLogout() {
        try {
            const response = await axiosInstance.post('users/token/blacklist/', {
                "refresh_token": localStorage.getItem("refresh_token"),
            });
            localStorage.removeItem('access_token');
            localStorage.removeItem('refresh_token');
            axiosInstance.defaults.headers['Authorization'] = null;
            this.setState({isAuthorized: authorized()});
            return response;
        }
        catch (e) {
            console.log(e);
        }
    };

    render() {
        if (this.state.isAuthorized) {
            return (
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <Link className={"nav-link"} to={"/myjokes"}>My jokes</Link>
                    <Link className={"nav-link"} to={"/newjoke/"}>Post new joke</Link>
                    <Link className={"nav-link"} to={"/getjoke/"}>Get my joke</Link>
                    <Link className={"nav-link"} to={"/generatejoke/"}>Generate joke</Link>
                    <form className="form-inline ml-12">
                        <button className={"btn btn-outline-success mr-sm-2"} onClick={this.handleLogout}>Logout</button>
                    </form>
                </nav>
            )
        }
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <Link className={"nav-link"} to={"/"}>Home</Link>
                <Link className={"nav-link"} to={"/login/"}>Login</Link>
                <Link className={"nav-link"} to={"/signup/"}>Signup</Link>
            </nav>
        )
    }
}

export default Navigator;