from django.conf.urls import url
from django.urls import path
from .views import index

# Catch simple https://site.com and more complicated https://site.com/anything URLs
urlpatterns = [
    path('', index),
    url(r'^.*/$', index)
]
