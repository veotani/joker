[![pipeline status](https://gitlab.com/veotani/joker/badges/master/pipeline.svg)](https://gitlab.com/veotani/joker/-/commits/master)
[![coverage report](https://gitlab.com/veotani/joker/badges/master/coverage.svg)](https://gitlab.com/veotani/joker/-/commits/master)

# Joker
Joker is a web application, which consists of server (REST API) and a client app (SPA React). It's goal is to provide a 
great user experience on joke management: create, store, generate and so on.

## Instructions

### docker-compose
You have to have Docker and docker-compose installed. Setup .env, where you mention:
 - `DJANGO_DEBUG`
 - `DJANGO_HOST`
 - `DJANGO_SECRET_KEY`
 - `SQL_DATABASE`
 - `SQL_ENGINE`
 - `SQL_HOST`
 - `SQL_PASSWORD`
 - `SQL_PORT`
 - `SQL_USER`
 - `MYSQL_ROOT_PASSWORD`

Then you simply run
`docker-compose up`. 

### Host
Before you start, make sure have Python 3.x (and also pip for sure) and node.js installed.
Go into root folder and then use these commands:
1) pip install -r requirements.txt
2) npm install
3) npm run build
4) python manage.py migrate
5) python manage.py runserver

Tested on python 3.7.3; npm 6.14.5; node 12.18.2. OS Windows 10.

This is pretty much it, but it's going to become a lot easier.