from django.db import models
from django.conf import settings


# Joke class
class Joke(models.Model):
    text = models.TextField()
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
