from django.urls import path

from .views import JokeView, AllJokesView

urlpatterns = [
    path('joke/', JokeView.as_view(), name='joke'),
    path('jokes/', AllJokesView.as_view(), name='jokes'),
]
