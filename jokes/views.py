import logging
import datetime
from helpers.ip import get_client_ip

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import get_user_model

from .models import Joke
from .helpers import get_user_from_request


logger = logging.getLogger("django.request")


# API for a single Joke object
class JokeView(APIView):
    def get(self, request):
        user = get_user_from_request(request)
        joke_id = request.query_params.get('joke_id', None)
        joke = Joke.objects.get(pk=joke_id)
        if joke.owner == user:
            logger.info('[GET]' + " " + str(datetime.datetime.now()) + " " + str(get_client_ip(request)) + " " + str(user.id))
            return Response({'text': joke.text}, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_403_FORBIDDEN)

    def post(self, request):
        user = get_user_from_request(request)
        joke_text = request.data['text']
        joke = Joke.objects.create(text=joke_text, owner=user)
        joke.save()
        logger.info('[POST]' + " " + str(datetime.datetime.now()) + " " + str(get_client_ip(request)) + " " + str(user.id))
        return Response(status=status.HTTP_200_OK)

    def put(self, request):
        user = get_user_from_request(request)
        joke_id = request.data['id']
        joke_text = request.data['text']
        joke = Joke.objects.get(pk=joke_id)
        if joke.owner == user:
            joke.text = joke_text
            joke.save()
            logger.info('[UPDATE]' + " " + str(datetime.datetime.now()) + " " + str(get_client_ip(request)) + " " + str(user.id))
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_403_FORBIDDEN)

    def delete(self, request):
        user = get_user_from_request(request)
        joke_id = request.data['id']
        joke = Joke.objects.get(pk=joke_id)
        if joke.owner == user:
            joke.delete()
            logger.info('[DELETE]' + " " + str(datetime.datetime.now()) + " " + str(get_client_ip(request)) + " " + str(user.id))
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_403_FORBIDDEN)


# API for a collection of Joke objects
class AllJokesView(APIView):
    def get(self, request):
        user = get_user_from_request(request)
        jokes = Joke.objects.filter(owner=user)
        result = [{"id": joke.pk, "text": joke.text} for joke in jokes]
        logger.info('[GET ALL]' + " " + str(datetime.datetime.now()) + " " + str(get_client_ip(request)) + " " + str(user.id))
        return Response(result, status=status.HTTP_200_OK)
