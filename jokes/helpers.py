from django.contrib.auth import get_user_model


# Return user object by request
def get_user_from_request(request):
    username = request.user.username
    user_model = get_user_model()
    user = user_model.objects.get(username=username)
    return user
