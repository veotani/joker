from django.urls import reverse
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken

from .models import Joke

TEST_USERNAME = "testun"
TEST_PASSWORD = "testpw"


class JokeTests(APITestCase):
    # Test posting a joke
    def test_post_joke(self):
        url = reverse('joke')
        token = get_test_user_token()
        data = {"text": "test_joke"}
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + str(token['access']))

        count_jokes_before_add = count_existing_jokes()
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        count_jokes_after_add = count_existing_jokes()

        self.assertEqual(count_jokes_after_add - 1, count_jokes_before_add)

    # Test getting a joke
    def test_get_joke(self):
        url = reverse('joke')
        token = get_test_user_token()
        test_joke = create_test_joke()
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + str(token['access']))
        response = self.client.get(url, data={'joke_id': test_joke.id})
        self.assertEqual(response.data['text'], test_joke.text)

    # Test updating a joke
    def test_put_joke(self):
        url = reverse('joke')
        token = get_test_user_token()
        test_joke = create_test_joke()
        new_text = "TEST"
        # Ensure old joke text is not set as TEST for some reason
        if test_joke.text == new_text:
            new_text = "NEW_TEST"

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + str(token['access']))
        self.client.put(url, {'id': test_joke.id, 'text': new_text})

        updated_joke = Joke.objects.get(pk=test_joke.pk)

        self.assertNotEqual(updated_joke.text, test_joke.text)
        self.assertEqual(new_text, updated_joke.text)

    def test_delete_joke(self):
        url = reverse('joke')
        token = get_test_user_token()
        test_joke = create_test_joke()
        count_jokes_before_delete = count_existing_jokes()
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + str(token['access']))
        self.client.delete(url, {'id': test_joke.id})
        count_jokes_after_delete = count_existing_jokes()

        deleted_joke = Joke.objects.filter(pk=test_joke.pk)

        self.assertEqual(count_jokes_after_delete + 1, count_jokes_before_delete)
        self.assertEqual(len(deleted_joke), 0)


'''
Helper functions for this test class
'''


# Return amount of stored jokes
def count_existing_jokes():
    jokes = Joke.objects.all()
    return len(jokes)


# Create test joke and return it for testing purposes
def create_test_joke():
    return Joke.objects.create(text="test joke", owner=get_test_user())


# Return test user object
def get_test_user():
    user_model = get_user_model()
    user = user_model.objects.filter(username=TEST_USERNAME)
    if user:
        return user[0]
    else:
        user = user_model.objects.create(username=TEST_USERNAME, password=TEST_PASSWORD)
        return user


# Return refresh and access jwt tokens for a test user
def get_test_user_token():
    user = get_test_user()
    refresh = RefreshToken.for_user(user)
    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }
