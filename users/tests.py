from django.urls import reverse
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken, BlacklistedToken, AccessToken
from django.db.utils import IntegrityError

from rest_framework.response import Response


class UserTests(APITestCase):
    # Create your tests here.
    def test_register_user(self):
        url = reverse('create_user')
        username = 'test'
        while check_username_exists(username):
            username += '1'
        self.client.post(url, data={'username': username, 'password': 'testpassword'}, format='json')

        user_model = get_user_model()
        self.assertEqual(len(user_model.objects.filter(username=username)), 1)

    # Test if it's able to create user with the same username
    def test_unique_username(self):
        url = reverse('create_user')
        user = create_new_user()
        try:
            self.client.post(url, data={'username': user.username, 'password': 'testpassword'}, format='json')
        except IntegrityError:
            return
        self.fail()

    # Test if blacklisting makes token invalid
    def test_blacklist(self):
        url = reverse('blacklist')
        user = create_new_user()
        refresh = RefreshToken.for_user(user)
        count_blacklisted_tokens_before_blacklist = len(BlacklistedToken.objects.filter(token__user=user))
        self.client.post(url, data={"refresh_token": str(refresh)})
        count_blacklisted_tokens_after_blacklist = len(BlacklistedToken.objects.filter(token__user=user))
        self.assertEqual(count_blacklisted_tokens_before_blacklist, count_blacklisted_tokens_after_blacklist - 1)

    # Test if it's possible to login
    def test_login(self):
        register = reverse('create_user')
        url = reverse('token_create')
        username = 'test'
        while check_username_exists(username):
            username += '1'
        self.client.post(register, {"username": username, "password": "testpassword"})
        response = self.client.post(url, {"username": username, "password": "testpassword"}, format='json')
        self.assertEqual(response.status_code, 200)


'''
Helper functions
'''


def check_username_exists(username):
    user_model = get_user_model()
    return bool(user_model.objects.filter(username=username))


def create_new_user():
    username = 'test'
    while check_username_exists(username):
        username += '1'
    user_model = get_user_model()
    user = user_model.objects.create(username=username, password='test')
    user.save()
    return user
