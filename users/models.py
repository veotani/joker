from django.contrib.auth.models import AbstractUser
from django.db import models


# Custom user model
class CustomUser(AbstractUser):
    pass

    def __str__(self):
        return self.username
