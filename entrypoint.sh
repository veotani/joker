#!/bin/sh

echo "Waiting for db..."

while ! nc -z $SQL_HOST $SQL_PORT; do
  sleep 0.1
done

echo "db started"

python manage.py migrate

exec "$@"